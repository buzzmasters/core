﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Atomic.Video.Service.Entities;
using Atomic.Video.Service.Helpers;
using Atomic.Video.Service.ViewModels;
using MongoDB.Driver;

namespace Atomic.Video.Service.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api/video")]
    public class VideoController : ApiController
    {
        private static readonly string _syncBucketName = ConfigurationManager.AppSettings["SyncBucketName"];
        private static readonly string _postersBucketName = ConfigurationManager.AppSettings["PostersBucketName"];

        [Route("{videoId}")]
        public VideoViewModel GetById(string videoId)
        {
            var videoCtx = new VideoContext();
            VideoEntity video = videoCtx.Videos.Find(v => v.Id.Equals(videoId)).FirstOrDefault();
            
            return video == null ? null : new VideoViewModel()
            {
                Id = video.Id,
                Description = video.Description,
                Likes = video.Likes,
                PublishDate = video.PublishDate,
                Title = video.Title,
                Url = video.Url
            };            
        }

        [Route("")]
        public List<VideoViewModel> Get(int? count = 8, string sortBy = null, bool ascending = true, string tag = null)
        {
            var videoCtx = new VideoContext();
            IFindFluent<VideoEntity, VideoEntity> videos;

            if (tag != null)
            {
                var filter = Builders<VideoEntity>.Filter.AnyIn(e => e.Tags, new List<string>() { tag });
                videos = videoCtx.Videos.Find(filter);
            }
            else
            {
                videos = videoCtx.Videos.Find(e => true);
            }
            if (sortBy != null)
            {
                var sortBuilder = ascending
                    ? Builders<VideoEntity>.Sort.Ascending(sortBy)
                    : Builders<VideoEntity>.Sort.Descending(sortBy);
                videos = videos.Sort(sortBuilder);
            }

            return videos.Limit(count).ToList().Select(video => new VideoViewModel()
            {
                Id = video.Id,
                PublishDate = video.PublishDate,
                Title = video.Title,
                PosterUrl = video.PosterUrl
            }).ToList();
        }

        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<HttpResponseMessage> UploadVideo()
        {
            //Uploading to S3
            StorageService storageService = new StorageService();

            if (!Request.Content.IsMimeMultipartContent())
            {
                this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }
            var provider = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartStreamProvider());
            NameValueCollection formData = provider.FormData;
            string fileName = StringHelpers.GetBase64NewGuid();

            IList<HttpContent> files = provider.Files;

            HttpContent videoFile = files[0];
            Stream videoFileStream = await videoFile.ReadAsStreamAsync();
            HttpContent posterFile = files[1];
            Stream posterFileStream = await posterFile.ReadAsStreamAsync();

            storageService.UploadFile(_syncBucketName, fileName, videoFileStream);
            storageService.UploadFile(_postersBucketName, fileName, posterFileStream);

            string videoFileUrl = StorageService.GetObjectUrl(_syncBucketName, fileName);
            string posterFileUrl = StorageService.GetObjectUrl(_postersBucketName, fileName);

            //Filling data in Mongo
            var videoEntity = new VideoEntity()
            {
                Id = fileName,
                Description = formData["Description"],
                Url = videoFileUrl,
                PublishDate = DateTime.UtcNow,
                Likes = 0,
                Title = formData["Title"],
                Tags = new List<string>() { formData["Tag"] },
                PosterUrl = posterFileUrl,
                AuthorId = Guid.Empty //TODO
            };
            new VideoContext().Videos.InsertOne(videoEntity);

            return this.Request.CreateResponse(HttpStatusCode.OK, new { id = fileName });
        }
    }
}
