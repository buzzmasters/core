﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atomic.Video.Service.Models
{
    public static class VideoTag
    {
        public static string Snowboarding => "Snowboarding";
        public static string Longboarding => "Longboarding";
        public static string Wakeboarding => "Wakeboarding";
        public static string Surfing => "Surfing";
    }
}