﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Atomic.Video.Service.Entities
{
    public class VideoEntity
    {
        [BsonId]
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("likes")]
        public int Likes { get; set; }

        [BsonElement("authorId")]
        public Guid AuthorId { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("publishDate")]
        public DateTime PublishDate { get; set; }

        [BsonElement("url")]
        public string Url { get; set; }

        [BsonElement("posterUrl")]
        public string PosterUrl { get; set; }

        [BsonElement("tags")]
        public IEnumerable<string> Tags { get; set; }
    }
}