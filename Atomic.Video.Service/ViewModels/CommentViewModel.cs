﻿using System;
using Newtonsoft.Json;

namespace Atomic.Video.Service.ViewModels
{
    public class CommentViewModel
    {
        [JsonProperty("author")]
        public CommentAuthorViewModel Author { get; set; }

        [JsonProperty("likes")]
        public int Likes { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }

    public class CommentAuthorViewModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("avatarUrl")]
        public string AvatarUrl { get; set; }
    }
}