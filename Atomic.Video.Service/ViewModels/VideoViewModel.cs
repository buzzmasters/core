﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Atomic.Video.Service.ViewModels
{
    public class VideoViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("likes")]
        public int Likes { get; set; }

        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("publishDate")]
        public DateTime PublishDate { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("posterUrl")]
        public string PosterUrl { get; set; }

        [JsonProperty("tags")]
        public IEnumerable<string> Tags { get; set; }
    }
}