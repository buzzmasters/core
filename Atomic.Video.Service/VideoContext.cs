﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using Atomic.Video.Service.Entities;

namespace Atomic.Video.Service
{
    public class VideoContext
    {
        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;

        static VideoContext()
        {
            string mongoDbConnectionString = ConfigurationManager.ConnectionStrings["mongo_database"].ConnectionString;
            _client = new MongoClient(mongoDbConnectionString);
            var databaseName = MongoUrl.Create(mongoDbConnectionString).DatabaseName;
            _database = _client.GetDatabase(databaseName);
        }

        public IMongoClient Client => _client;

        public IMongoCollection<Entities.VideoEntity> Videos
        {
            get
            {
                string videosCollectionName = ConfigurationManager.AppSettings["videos_collection_name"];
                return _database.GetCollection<Entities.VideoEntity>(videosCollectionName);
            }
        }
    }
}