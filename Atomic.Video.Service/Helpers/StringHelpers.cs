﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atomic.Video.Service.Helpers
{
    public static class StringHelpers
    {
        public static string GetBase64NewGuid()
        {
            Guid guid = Guid.NewGuid();
            string guidString = Convert.ToBase64String(guid.ToByteArray());
            guidString = guidString
                .Replace("=", "")
                .Replace("+", "")
                .Replace("\\", "")
                .Replace("/", "");

            return guidString;
        }
    }
}