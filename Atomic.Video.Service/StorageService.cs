﻿using System;
using System.Configuration;
using System.IO;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace Atomic.Video.Service
{
    public class StorageService
    {
        private readonly IAmazonS3 _client = null;

        public StorageService()
        {
            string accessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            string secretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            if (this._client == null)
            {
                this._client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), RegionEndpoint.USWest2);
            }
        }

        public bool UploadFile(string awsBucketName, string key, Stream stream)
        {
            var uploadRequest = new TransferUtilityUploadRequest
            {
                InputStream = stream,
                BucketName = awsBucketName,
                CannedACL = S3CannedACL.PublicRead,
                Key = key
            };

            TransferUtility fileTransferUtility = new TransferUtility(this._client);
            fileTransferUtility.Upload(uploadRequest);
            return true;
        }

        public string GeneratePreSignedURL(string awsBucketName, string key, int expireInSeconds)
        {
            string urlString = string.Empty;
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
            {
                BucketName = awsBucketName,
                Key = key,
                Expires = DateTime.Now.AddSeconds(expireInSeconds)
            };

            urlString = this._client.GetPreSignedURL(request);
            return urlString;
        }

        public GetObjectMetadataResponse GetObjectMetadata(string awsBucketName, string key)
        {
            return _client.GetObjectMetadata(awsBucketName, key);
        }

        public static string GetObjectUrl(string awsBucketName, string key)
        {
            return $"http://{awsBucketName}.s3.amazonaws.com/{key}";
        }
    }
}