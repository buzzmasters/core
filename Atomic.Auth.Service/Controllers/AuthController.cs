﻿using System.Net.Http;
using System.Web.Http;
using Atomic.Auth.Service.Entities;
using Atomic.Auth.Service.Models;

namespace Atomic.Auth.Service.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : ApiController
    {
        [HttpPost]
        [Route("register")]
        public void Register([FromBody]UserModel userModel)
        {
            var authRepository = new MongoAuthContext();

            authRepository.Register(new User()
            {
                Email = userModel.Email,
                Password = userModel.Password,
                Username = userModel.Username
            });
        }
    }
}
