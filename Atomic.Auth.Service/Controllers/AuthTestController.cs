﻿using System.Collections.Generic;
using System.Web.Http;

namespace Atomic.Auth.Service.Controllers
{
    [Authorize]
    public class AuthTestController : ApiController
    {
        public List<string> Get()
        {
            return new List<string>()
            {
                "asdfas",
                "asdfasd",
                "asasdf"
            };
        } 
    }
}
