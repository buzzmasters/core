﻿using Newtonsoft.Json;

namespace Atomic.Auth.Service.Models
{
    [JsonObject]
    public class UserModel
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}