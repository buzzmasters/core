﻿namespace Atomic.Auth.Service.Models
{
    public enum RegistrationStatus
    {
        AlreadyExists,
        Success
    }
}