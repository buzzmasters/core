﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Atomic.Auth.Service.Entities
{
    public class RefreshToken
    {
        [BsonId]
        public string Id { get; set; }

        [BsonElement("subject")]
        public string Subject { get; set; }

        [BsonElement("clientId")]
        public string ClientId { get; set; }

        [BsonElement("issuedUtc")]
        public DateTime IssuedUtc { get; set; }

        [BsonElement("expiresUtc")]
        public DateTime ExpiresUtc { get; set; }

        [BsonElement("protectedTicket")]
        public string ProtectedTicket { get; set; }
    }
}