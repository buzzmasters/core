﻿namespace Atomic.Auth.Service.Entities
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}