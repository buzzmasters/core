﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace Atomic.Auth.Service.Entities
{
    public class User
    {
        [BsonId]
        public Guid Id { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("email")]
        public string Email { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("email_confirmed")]
        public string EmailConfirmed { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("username")]
        public string Username { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("password")]
        public string Password { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("roles")]
        public IEnumerable<string> Roles { get; set; }
    }
}