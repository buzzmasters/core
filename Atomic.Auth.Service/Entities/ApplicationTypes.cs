﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Atomic.Auth.Service.Entities
{
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };
}