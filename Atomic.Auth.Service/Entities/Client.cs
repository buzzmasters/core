﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Atomic.Auth.Service.Entities
{
    public class Client
    {
        [BsonId]
        public string Id { get; set; }

        [BsonElement("secret")]
        public string Secret { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("applicationType")]
        public ApplicationTypes ApplicationType { get; set; }

        [BsonElement("isActive")]
        public bool IsActive { get; set; }

        [BsonElement("refreshTokenLifeTime")]
        public int RefreshTokenLifeTime { get; set; }

        [BsonElement("allowedOrigin")]
        public string AllowedOrigin { get; set; }
    }
}