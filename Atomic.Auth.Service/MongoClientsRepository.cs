﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Atomic.Auth.Service.Entities;
using MongoDB.Driver;

namespace Atomic.Auth.Service
{
    public class MongoClientsRepository
    {
        private MongoAuthContext _ctx;

        public MongoClientsRepository()
        {
            _ctx = new MongoAuthContext();
        }

        public Client FindClient(string clientId)
        {
            var client = _ctx.Clients.Find(c=>c.Id == clientId).FirstOrDefault();

            return client;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {
            var existingToken = _ctx.RefreshTokens.Find(r => r.Subject == token.Subject && r.ClientId == token.ClientId).SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            try
            {
                await _ctx.RefreshTokens.InsertOneAsync(token);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> RemoveRefreshToken(string refreshTokenId)
        {
            _ctx.RefreshTokens.DeleteOne(t => t.Id == refreshTokenId);
            return true;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _ctx.RefreshTokens.DeleteOne(t => t.Id == refreshToken.Id);
            return true;
        }

        public async Task<RefreshToken> FindRefreshToken(string refreshTokenId)
        {
            var refreshToken = await _ctx.RefreshTokens.FindAsync(t => t.Id == refreshTokenId);

            return refreshToken.FirstOrDefault();
        }

        public List<RefreshToken> GetAllRefreshTokens()
        {
            return _ctx.RefreshTokens.Find(e => true).ToList();
        }
    }
}