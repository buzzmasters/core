﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atomic.Video.Service.Entities;
using MongoDB.Driver;
using NUnit.Framework;

namespace Atomic.Video.Service.Tests
{
    [Explicit]
    public class MongoInitializerTests
    {
        [Test]
        public void FillMongoWithTestData()
        {
            var ctx = new VideoContext();

            ctx.Videos.DeleteMany(e => true);

            ctx.Videos.InsertMany(new List<Entities.VideoEntity>()
            {
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 16,
                    PublishDate = DateTime.Today,
                    Title = "Old VHS Footage of Shane McConke",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Old_VHS_Footage_of_Shane_McConkey_BASE_Jumping_-_McConkey.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Shane McConkey First Ski Basejump",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Shane_McConkey%27s_First_Ski_BASE_Jump_at_Lover%27s_Leap_-_McConkey.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Downhill Longboarding",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Downhill+Longboarding+%E2%98%85+BEST+LONGBOARDING+DOWNHILL+(HD)+%5BAdrenaline+Channel%5D.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "GoPro- Wakeboarding The Philippines",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/GoPro-+Wakeboarding+The+Philippines+with+Dylan+Mitchell.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Shane McConkey First Ski Basejump",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Shane_McConkey%27s_First_Ski_BASE_Jump_at_Lover%27s_Leap_-_McConkey.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Shane McConkey First Ski Basejump",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Shane_McConkey%27s_First_Ski_BASE_Jump_at_Lover%27s_Leap_-_McConkey.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Shane McConkey First Ski Basejump",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Shane_McConkey%27s_First_Ski_BASE_Jump_at_Lover%27s_Leap_-_McConkey.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Shane McConkey First Ski Basejump",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Shane_McConkey%27s_First_Ski_BASE_Jump_at_Lover%27s_Leap_-_McConkey.mp4"
                },
                new Entities.VideoEntity()
                {
                    Id = Helpers.StringHelpers.GetBase64NewGuid(),
                    AuthorId = Guid.NewGuid(),
                    Description = "Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor Lorem impsum dolor",
                    Likes = 3,
                    PublishDate = new DateTime(2012, 10, 4, 5, 1, 0),
                    Title = "Shane McConkey First Ski Basejump",
                    Url = "https://s3.amazonaws.com/elive-vids-samples/Shane_McConkey%27s_First_Ski_BASE_Jump_at_Lover%27s_Leap_-_McConkey.mp4"
                }
            });
        }
    }
}
