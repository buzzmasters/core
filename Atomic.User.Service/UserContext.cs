﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Atomic.User.Service.Entities;
using MongoDB.Driver;

namespace Atomic.User.Service
{
    public class UserContext
    {
        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;

        static UserContext()
        {
            string mongoDbConnectionString = ConfigurationManager.ConnectionStrings["mongo_database"].ConnectionString;
            _client = new MongoClient(mongoDbConnectionString);
            var databaseName = MongoUrl.Create(mongoDbConnectionString).DatabaseName;
            _database = _client.GetDatabase(databaseName);
        }

        public IMongoClient Client => _client;

        public IMongoCollection<UserEntity> Videos
        {
            get
            {
                string videosCollectionName = ConfigurationManager.AppSettings["users_collection_name"];
                return _database.GetCollection<UserEntity>(videosCollectionName);
            }
        }
    }
}